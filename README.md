# samvera-test
## Contents
- [Links](#links)
- [hydra-head](#hydra-head)
- [hyrax](#hyrax)
    - [vagrant ubuntu (from samvera-labs)](#vagrant-ubuntu-from-samvera-labs)
    - [vagrant centos](#vagrant-centos)
- [hyku](#hyku)

## Links
* Samvera Main Page: https://samvera.org/
* Samvera Wiki: https://wiki.duraspace.org/display/samvera/Samvera
* Technical Framework and its Parts: https://wiki.duraspace.org/display/samvera/Technical+Framework+and+its+Parts
* Distinction hyrax / hyku: https://wiki.duraspace.org/pages/viewpage.action?pageId=85530575
* Samvera Git Repository: https://github.com/samvera
* Samvera-Labs Git Repository: https://github.com/samvera-labs
* A Guide For The Perplexed Samvera Developer: http://samvera.github.io/

## hydra-head
this section mainly summarizes the steps of the [Dive-into-Hydra tutorial](https://github.com/samvera/hydra/wiki/Dive-into-Hydra).
for more details check the Dive-into-Hydra tutorial itself or the other repositories on https://github.com/samvera/

1. `git clone https://gitlab.com/dsbrng25b/samvera-test.git`
1. `cd samvera-test`
1. `vagrant up`
1. `vagrant ssh`
1. `gem install hydra`
1. `rails new hydra_test`
1. `cd hydra_test`
1. `echo "gem 'therubyracer', platforms: :ruby" >> Gemfile`
1. `echo "gem 'hydra'" >> Gemfile`
1. `bundle update`
1. `rails generate hydra:install`

### create a model
* create a model in `app/models/book.rb`

```ruby
class Book < ActiveFedora::Base
  property :title, predicate: ::RDF::Vocab::DC.title, multiple: false do |index|
    index.as :stored_searchable
  end
  property :author, predicate: ::RDF::Vocab::DC.creator, multiple: false do |index|
    index.as :stored_searchable
  end
end
```
* `rails generate scaffold Book title:string author:string`

    When it asks you whether to overwrite `app/models/book.rb`, enter `n` and hit enter.
    Then either remove the generated db migration files or run `rails db:migrate`.

* disable access controls in `app/controllers/catalog_controller.rb`

    uncomment `before_filter :enforce_show_permissions, only: :show` and add the following line.
```ruby
# This filter applies the hydra access controls
# before_action :enforce_show_permissions, only: :show
# This applies appropriate access controls to all solr queries
Hydra::SearchBuilder.default_processor_chain -= [:add_access_controls_to_solr_params]
```

### start the app
the following task starts the Fedora Commons Repository, Solr and Rails:
```
rails hydra:server
```

then go to http://localhost:3000/ and you should see your app.

### create a book
#### web ui
go to http://localhost:3000/books to create a new book over the web ui.
the `/books` endpoint was created by running the `rails generate scaffold` command.

#### rails console
start the `rails console` and run the following code:
```ruby
b = Book.new(id: 'test-1')
b.title = "Also sprach Zarathustra"
b.author = "Friedrich Nietzsche"
b.save
```

## hyrax
the [getting started page](https://samvera.org/communication/getting-started/) on the samvera website says:
> The easiest way to get started developing with Samvera is to install and configure Hyrax. While it is possible to create your own Samvera application by assembling the right components, and this has been a common practice in the Samvera Community in the past, ongoing maintenance of home grown solutions will be more expensive than sharing maintenance costs with the rest of the Community. ...

### vagrant ubuntu (from samvera-labs)
with the samvera-vagrant project you can setup hyrax in a vagrant box. for detailed reference check the [README](https://github.com/samvera-labs/samvera-vagrant) page of the project.
* `git clone https://github.com/samvera-labs/samvera-vagrant.git`
* `cd samvera-vagrant`
* `vagrant up`
* `vagrant ssh`
* `cd /home/ubuntu/hyrax-demo`
* `bundle exec rake demo`
* then go to http://localhost:3000/
The samvera-vagrant box also contains hyku. To start hyku run `bundle exec rake demo` in `/home/ubuntu/hyku-demo`.

### vagrant centos
The following instructions desribe the setup of a hyrax app in a centos vagrant box.
The box does not yet meet the requirements to run all the features of hyrax.
The following components are not yet installed:
* ffmpeg

To setup hyrax run the following commands.
This process is described in detail in the [README](https://github.com/samvera/hyrax) of the hyrax repository.
* `git clone https://gitlab.com/dsbrng25b/samvera-test.git`
* `cd samvera-test`
* `vagrant up`
* `vagrant ssh`
* `ansible-playbook -i "localhost," -c local /vagrant/ansible/sample_app.yml`
* `cd my_app`
* (optional) change ActiveJob adapter to inline in file `config/application.rb`

```ruby
class Application < Rails::Application
  # ...
  config.active_job.queue_adapter = :inline
  # ...
end
```
* `rails generate hyrax:work Work`
* `rails hydra:server`
* `rails hyrax:default_admin_set:create`
* set up admin user: https://github.com/samvera/hyrax/wiki/Setting-up-test-app-for-workflow


## hyku

* `git clone https://github.com/samvera-labs/hyku.git` 
* `cd hyku`
* `docker-compose up -d`

to create an admin user follow the instractions on https://github.com/samvera-labs/hyku/wiki/Create-super-admin-user

then go to http://localhost:8080/
